/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<getopt.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>



#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { 0,         0,                 0,  0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hvi:o:";



/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
 
        exit(EXIT_SUCCESS);
      default :
        break;
    }
  } 

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  /*if (bin_input_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }*/


  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %s\n\n%-8s: %d\n", 
          "input",   bin_input_param, 
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point
  int p[2];
  int p2[2];
  if(pipe(p) == -1){
    fprintf(stderr, "Pipe error\n");
    exit(-1);
  }
  if(pipe(p2) == -1){
    fprintf(stderr, "Pipe 2 error\n");
    exit(-1);
  }
  int f = fork();
  if(f == -1){
    fprintf(stderr, "Fork error\n");
    exit(-1);
  }
  int res;
  if (f == 0) {
    close(p[1]);
    dup2(p[0], STDIN_FILENO);

    close(p2[0]);
    dup2(p2[1], STDOUT_FILENO);
    //dup2(p2[1], STDERR_FILENO);
    execlp("grep","grep","^root",NULL);

    close(p[0]);
    close(p2[1]);
    exit(EXIT_SUCCESS);
  }
  else {
    int f2 = fork();
    if(f2 == 0){
      close(p[0]);
      dup2(p[1],STDOUT_FILENO);

      execlp("ps","ps","eaux",NULL);
      close(p[1]);
      wait(&res);
      exit(EXIT_SUCCESS);
    }
    else{
      close(p[0]);
      close(p[1]);
      close(p2[1]);

      char result[1024];
      int length = read(p2[0],result,sizeof(result));
      close(p2[0]);

      result[length] = '\0';
      if(length > 0) printf("root connecté\n");
    }
  }

  // Freeing allocated data
  free_if_needed(bin_input_param);


  return EXIT_SUCCESS;
}
