/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<signal.h>
#include <unistd.h>
#include <time.h>

#include<getopt.h>


#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hvi:o:";

int continue_loop = 1;
int child;
int parent;
int child_points = 0;
int parent_points = 0;

void print_score() {
    printf("Current score:\n   Parent: %d\n   Child: %d\n", parent_points, child_points);
}
void sig_hand(int signum) {
    if(rand() % 4 != 3) printf("Ping!\n");
    else {
        printf("Parent MISSES!\n");
        child_points++;
        print_score();
        kill(child, SIGUSR2);
    }
    sleep(1);
    if (child_points == 13) {
        printf("Child wins!\n");
        printf("Final score:\n   Parent: %d\n   Child: %d\n", parent_points, child_points);
        return;
    }

    kill(child, SIGUSR1);
}

void sig_hand_add_points(int signum) { //Ces fonctions "add points" permettent de synchroniser les scores, le parent et l'enfant ne travaillant pas avec les m�mes zones m�moire "child/parent points"
    parent_points++;
}



void sig_hand_child(int signum) {
    
    
    if(rand()%4 != 3) printf("Pong!\n");
    else {
        printf("Child MISSES!\n");
        parent_points++;
        print_score();
        kill(parent, SIGUSR1);
    }
    sleep(1);

    if (parent_points == 13) {
        printf("Parent wins!\n");
        printf("Final score:\n   Parent: %d\n   Child: %d\n", parent_points, child_points);
        return;
    }

    kill(parent, SIGUSR2);
}

void sig_hand_child_add_points(int signum) {
    child_points++;
}


void sig_stop(int signum) {
    continue_loop = 0;
    printf("Program exited by signal SIGTERM\n");
}

/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  /*char* bin_input_param = NULL;
  char* bin_output_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
 
        exit(EXIT_SUCCESS);
      default :
        break;
    }
  } 

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  /*
  if (bin_input_param == NULL || bin_output_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }
  */

  // Printing params
  /*dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %s\n%-8s: %d\n",
          "input",   bin_input_param, 
          "output",  bin_output_param, 
          "verbose", is_verbose_mode);*/

  // Business logic must be implemented at this point

  
  child = fork();

  if (child == 0) {
      srand(time(0));
      parent = getppid();

      sigset_t sig_proc_child;
      struct sigaction action_child;

      sigemptyset(&sig_proc_child);
      action_child.sa_mask = sig_proc_child;
      action_child.sa_flags = 0;
      action_child.sa_handler = sig_hand_child;
      sigaction(SIGUSR1, &action_child, 0);

      sigset_t sig_proc_child_add_points;
      struct sigaction action_child_add_points;

      sigemptyset(&sig_proc_child_add_points);
      action_child_add_points.sa_mask = sig_proc_child_add_points;
      action_child_add_points.sa_flags = 0;
      action_child_add_points.sa_handler = sig_hand_child_add_points;
      sigaction(SIGUSR2, &action_child_add_points, 0);


      kill(parent, SIGUSR2);
      while (parent_points<13 && child_points<13) {  }
  }
  else { 
      srand(time(0));
      sigset_t sig_proc;
      struct sigaction action;

      sigemptyset(&sig_proc);
      action.sa_mask = sig_proc;
      action.sa_flags = 0;
      action.sa_handler = sig_hand;
      sigaction(SIGUSR2, &action, 0);


      sigset_t sig_proc_add_points;
      struct sigaction action_add_points;

      sigemptyset(&sig_proc_add_points);
      action_add_points.sa_mask = sig_proc_add_points;
      action_add_points.sa_flags = 0;
      action_add_points.sa_handler = sig_hand_add_points;
      sigaction(SIGUSR1, &action_add_points, 0);

      while (parent_points < 13 && child_points < 13) {  }

  }


  // Freeing allocated data
  //free_if_needed(bin_input_param);
  //free_if_needed(bin_output_param);


  return EXIT_SUCCESS;
}
